<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GroupInvitePlayerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add(
            'Player', 
            TextType::class,
            [
                'label' => 'Players*: ',
                'help' => 'Add list of players separated by ";", for example: player1@local.com; player2@local.com',
                'attr' => [
                    'class' => 'form-control', 
                ],
                'required'   => TRUE,
            ]
        );
        $builder->add(
            'Save', 
            SubmitType::class, 
            [
                'label' => 'Add players',
                'attr' => [
                    'class' => 'btn btn-primary'
                ],
            ]
        );
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
