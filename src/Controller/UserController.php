<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\GroupInscriptionRepository;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index(GroupInscriptionRepository $groupInscriptionRepository): Response
    {
        $user = $this->getUser();

        $campaign = $groupInscriptionRepository->findOneByUser($user);

        
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
            'isVerified' => $user->isVerified(),
        ]);
    }
}
