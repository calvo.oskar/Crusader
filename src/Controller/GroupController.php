<?php

namespace App\Controller;

use App\Entity\Group;
use App\Form\GroupFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\GroupRepository;

use \DateTime;

class GroupController extends AbstractController
{
    /**
     * @Route("/group", name="group")
     */
    public function index(): Response
    {
        return $this->render('group/index.html.twig', [
            'controller_name' => 'GroupController',
        ]);
    }

    /**
     * @Route("/group/add", name="group_add")
     */
    public function new(Request $request): Response
    {
        $group = new Group();
        $form = $this->createForm(GroupFormType::class, $group);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $fecha = new DateTime('now');
            
            $group->setTitle( $form->get('Title')->getData());
            $group->setDescription($form->get('Description')->getData());
            $group->setOwner($this->getUser());
            $group->setCreated($fecha);
            $group->setUpdated($fecha);

            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return new Response ('Campaing has been created, now you have to invited players');
        }

        return $this->render('group/group_add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/group/{id}", name="group_view")
     */
    public function view(int $id, GroupRepository $grouprepository): Response 
    {

        $group = $grouprepository->findOneById($id);

        return $this->render('group/group.html.twig', [
            'controller_name' => 'GroupController',
            'title' => $group->getTitle(),
            'description' => $group->getDescription(),
            'id' => $group->getId(),
        ]);
    }

    /**
     * @Route("/group/{id}/edit", name="group_edit")
     */
    public function edit(int $id, GroupRepository $grouprepository, Request $request): Response 
    {
        $group = $grouprepository->findOneById($id);

        $form = $this->createForm(GroupFormType::class, $group);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $fecha = new DateTime('now');

            $group->setTitle( $form->get('Title')->getData());
            $group->setDescription($form->get('Description')->getData());
            $group->setUpdated($fecha);            
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            $this->addFlash(
                'notice',
                'Yours group has been updated!'
            );

            return  $this->redirect('/group/'. $id, 301);
        }

        return $this->render('group/group_edit.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
