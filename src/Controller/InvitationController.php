<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\InvitationRepository;
use App\Repository\GroupRepository;
use App\Repository\GroupInscriptionRepository;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Form\InvitationRegisterType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session;


class InvitationController extends AbstractController
{

    /**
     * @Route("/invitation/{token}", name="validate_invitation")
     */
    public function getInvitation($token, InvitationRepository $invitationRepository, Request $request, UserPasswordEncoderInterface $passwordEncoder,    GroupRepository $groupRepository, GroupInscriptionRepository $groupInscriptionRepository, UserRepository $userRepository): Response
    {
        $invitation = $invitationRepository->findOneByToken($token);
        
            $user = new user();
            if(!isset($form)){
                $user->setEmail($invitation->getInvitedEmail());;
                //$user->setPassword('');
            }
            $form = $this->createForm(InvitationRegisterType::class, $user);
            $form->handleRequest($request);
   
            if($form->isSubmitted() && $form->isValid()) {
                $password = $passwordEncoder->encodePassword($user,               $form->get('plainPassword')->getData());

                // Crear usuarios
                $date = new \Datetime();
                $newUser = $userRepository->save(                 $invitation->getInvitedEmail(), $password, 1, $date);              

                // Guardar en Group_invitation como registrdo.
                $group = $groupRepository->findOneById($invitation->getCampaign()->getId());
                $groupInscription = $groupInscriptionRepository->save($group, $newUser, 1, $date);

                // Borrar invitación
                $invitationRepository->delete($invitation);

                //Go to login
                $this->addFlash(
                    'notice',
                    'You can login to star playing now!'
                );
                return $this->redirectToRoute('app_login');

                
            }

        return $this->render('invitation/invitation_register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
