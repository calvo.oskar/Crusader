<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\GroupInvitePlayerType;
use App\Repository\UserRepository;
use App\Repository\GroupRepository;
use App\Repository\GroupInscriptionRepository;
use App\Repository\InvitationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use \DateTime;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;





class GroupInscriptionController extends AbstractController
{
    /**
     * @Route("/group/{id}/inscription", name="group_inscription")
     */
    public function index(int $id, GroupRepository $groupRepository,       UserRepository $userRepository, GroupInscriptionRepository $groupInscriptionRepository, InvitationRepository $invitationRepository,  MailerInterface $mailer, Request $request): Response
    {

        $group = $groupRepository->findOneById($id);
     
        $form = $this->createForm(GroupInvitePlayerType::class);
        $form->handleRequest($request);
      
        if($form->isSubmitted() && $form->isValid()) {
            $rawPlayers = $form->get('Player')->getData();

            $players = explode(';', $rawPlayers);
            $fecha = new DateTime('now');

            foreach($players as $player) {
                $player = trim($player);    
                if(filter_var($player, FILTER_VALIDATE_EMAIL)) {
                    
                    $isRegistered = $userRepository->findOneByEmail($player);
                                        
                    if (is_object($isRegistered) && $isRegistered instanceof User){
                        $hasJoined = $groupInscriptionRepository->findOneByUserCampaign($isRegistered, $group);
                        if (empty($hasJoined)) {
                            $groupInscriptionRepository->save($group, $isRegistered,0, $fecha);
                        }
                    }else{

                        // Comprobar si el correo invitado ya esta en esta campaña
                        $isYetInvited = $invitationRepository->findOneByMailCampaign($player, $group);
                        if(empty($isYetInvited)) {

                            // generar ulid
                            $ulid = new Ulid();
                            // TODO guardar en invitacion    
                            $invitationRepository->save($this->getUser(), $group,$player,0,$ulid->toRfc4122());

                            // TODO enviar correo electrónico
                            $email = (new TemplatedEmail())
                                ->from(new Address('crusadercampaing@gmail.com', 'Crusader'))
                                ->to($player)
                                ->subject('Yo have invited to the campaing '. $group->getTitle())
                                ->htmlTemplate('invitation/invitation_email.html.twig')
                                ->context([
                                   'campaignTitle' => $group->getTitle(),
                                   'invitationToken' =>  $ulid->toRfc4122(),
                                ])
                            ;   
                            $mailer->send($email);
                        }    
                    }
                }
            }
            $this->addFlash('notice','Yours players have been invited!');
            return  $this->redirect('/group/'. $id, 301);
        }


        return $this->render('group_inscription/group_invite.html.twig', [
            'form' => $form->createView(),
            'group' => $group,
        ]);
    }
}

