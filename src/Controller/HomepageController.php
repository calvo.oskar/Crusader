<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        $user = $this->getUser();

        return (isset($user->id)) ? $this->redirectToRoute('user') : $this->redirectToRoute('app_login');
        // TODO delete
        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
        ]);

    }
}
