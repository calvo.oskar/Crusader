<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\User;
use App\DataFixtures\UserFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;



class GroupFixtures extends Fixture implements  FixtureGroupInterface, DependentFixtureInterface
{

    public const Group = 'group';
    public function load(ObjectManager $manager)
    {

        $description= "
        <p>
        Lorem Ipsum is simply dummy text of the printing and typesetting 
        industry. Lorem Ipsum has been the industry 's standard dummy 
        text ever since the 1500s, when an unknown printer took a galley 
        of type and scrambled it to make a type specimen book. </p>
        <p>It has survived not only five centuries, but also the leap 
        into electronic typesetting, remaining essentially unchanged. 
        It was popularised in the 1960s with the release of Letraset 
        sheets containing Lorem Ipsum passages, and more recently with 
        desktop publishing software like Aldus PageMaker including 
        versions of Lorem Ipsum.</p>
        ";

        $owner = $this->getReference(UserFixtures::GROUP_OWNER);

        $date = new \Datetime();

        for ($i =0; $i <= 5; $i++){
            $group = new Group();
            $group->setTitle('Campaña 1');
            $group->setDescription($description);
            $group->setOwner($owner);
            $group->setcreated($date);
            $group->setupdated($date);
            $manager->persist($group);

        }

        $manager->flush();

        $this->addReference(self::GROUP, $group);
    }

    public static function getGroups(): array
    {
        return ['group'];
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }        
}
