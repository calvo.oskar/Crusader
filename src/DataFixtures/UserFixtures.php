<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;



class UserFixtures extends Fixture implements FixtureGroupInterface
{
    private $passwordEncoder;
    public const GROUP_OWNER = 'group-owner';


    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $date = new \Datetime();
        // $product = new Product();
        // $manager->persist($product);
        $user = new User();
        $user->setEmail('dorn@local.es');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'user000'
            )
        );
        $user->setCreated($date);
        $user->setUpdated($date);
        $user->setIsVerified(1);
        $user->setPicture('https://static.wikia.nocookie.net/eswarhammer40k/images/0/03/Retrato_Rogal_Dorn_Horus_Heresy_III_Extermination.jpg/revision/latest/scale-to-width-down/300?cb=20140411124252');
        $manager->persist($user);

        $user = new User();
        $user->setEmail('perturabo001@local.es');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'user001'
            ));  
        $user->setPicture('https://static.wikia.nocookie.net/eswarhammer40k/images/7/79/Perturabo_Guerreros_de_Hierro_Forge_World_ilustraci%C3%B3n.jpg/revision/latest?cb=20180212143236');
        $user->setCreated($date);
        $user->setUpdated($date);
        $user->setIsVerified(1);            
        $manager->persist($user);


        $user = new User();
        $user->setEmail('lion@local.es');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'user002'
            ));
        $user->setPicture('https://static.wikia.nocookie.net/eswarhammer40k/images/4/49/Primarca_Lion_El%27Jonson_Wikihammer_40k.jpg/revision/latest?cb=20120820150506');
        $user->setCreated($date);
        $user->setUpdated($date);
        $user->setIsVerified(1);            
        $manager->persist($user);

        $user = new User();
        $user->setEmail('ferrus@local.es');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'user003'
            ));
        $user->setPicture('https://static.wikia.nocookie.net/eswarhammer40k/images/8/82/Primarca_Ferrus_Manus_Manos_de_Hierro_Bosquejo_Legiones_Astartes_Gran_Cruzada_Wikihammer.jpg/revision/latest?cb=20140131120856');
        $user->setCreated($date);
        $user->setUpdated($date);
        $user->setIsVerified(1);            
        $manager->persist($user);

        $manager->flush();

        $this->addReference(self::GROUP_OWNER, $user);
    }

    public static function getGroups(): array
    {
        return ['user', 'group'];
    }

}
