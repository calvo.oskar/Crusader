<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Group;
use App\Entity\GroupInscription;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;



class GroupInscriptionFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $date = new \Datetime();

        for($i = 0; $i <= 5; $i++){
            $groupInscription = new GroupInscription();
            $user = $manager->getRepository(User::Class)->findOneById($i);
            var_dump($user);
            $groupInscription->setUser(
            );
            $groupInscription->setCampaign(
                $manager->getRepository(Group::Class)->findOneById(1)
            );

            $isVerified = ($i % 2 == 0) ? 0 : 1;
            $groupInscription->setIsVerified( $isVerified);
            $groupInscription->setJoined($date);
            // $manager->persist($groupInscription);
        }

//        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['groupinscription'];
    }   

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            GroupFixtures::class,
        );
    }   
}
