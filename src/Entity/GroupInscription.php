<?php

namespace App\Entity;

use App\Repository\GroupInscriptionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GroupInscriptionRepository::class)
 */
class GroupInscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $Campaign;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $joined;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getCampaign(): ?Group
    {
        return $this->Campaign;
    }

    public function setCampaign(?Group $Campaign): self
    {
        $this->Campaign = $Campaign;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getJoined(): ?\DateTimeInterface
    {
        return $this->joined;
    }

    public function setJoined(?\DateTimeInterface $joined): self
    {
        $this->joined = $joined;

        return $this;
    }
}
