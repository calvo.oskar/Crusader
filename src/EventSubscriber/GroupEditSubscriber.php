<?php

namespace App\EventSubscriber;

use App\Repository\GroupRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
//use Symfony\Component\HttpFoundation\Response;






class GroupEditSubscriber implements EventSubscriberInterface
{
    private $session;

    public function __construct(TokenStorageInterface $tokenStorage, GroupRepository $groupRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->groupRepository = $groupRepository;
        //$this->accessDeniedException = $accessDeniedException;
    }

    /**
     * $uri[0]  
     * $uri[1] group
     * $uri[2] $group_id
     * $uri[3] edit
     */
    public function onKernelRequest(RequestEvent $event)
    {

        if (!$event->isMasterRequest()){
            return;
        }

        $uri = explode('/',$event->getRequest()->getrequestUri());
        
        if($uri[1] == 'group' && isset($uri[3]) && $uri[3] == 'edit' && is_numeric($uri[2])) {

            // Si es usuario anónimo.  
            // TODO revisar porque seguridad no funciona en estas rutas
            if( $this->tokenStorage->getToken()->getUser() === 'anon.')
            {
                throw new AccessDeniedException();
            }

            // Si es usuario autententica pero no es el autor
            $owner = $this->groupRepository->findOneById($uri[2])->getOwner();
            if($this->tokenStorage->getToken()->getUser()->getId() !== $owner->getId()) 
            {
                // devolvemos un 403
                throw new AccessDeniedException('Access Denied');
                // TODO investigar más los 403
                //$event->setResponse(new Response(NULL, 403));
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'onKernelRequest',
        ];
    }
}
