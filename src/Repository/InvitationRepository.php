<?php

namespace App\Repository;

use App\Entity\Invitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Invitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invitation[]    findAll()
 * @method Invitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Invitation::class);
        $this->entityManager = $entityManager;
    }

    // /**
    //  * @return Invitation[] Returns an array of Invitation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Invitation
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneByToken($token): ?Invitation
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.token = :val')
            ->setParameter('val', $token)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneByMailCampaign($mail, $campaign): ?array
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.invitedEmail = :mail')
            ->setParameter('mail', $mail)
            ->andWhere('i.campaign = :campaign')
            ->setParameter('campaign', $campaign)
            ->getQuery()
            ->getScalarResult();
    }

    public function delete($invitation)
    {
        $this->_em->remove($invitation);
        $this->_em->flush();
    }

    public function save($inviter, $group, $player_email, $status, $token): ?Invitation
    {
        $newInvitation = new Invitation();
        $invitation->setInviter($inviter);
        $invitation->setCampaign($group);
        $invitation->setInvitedEmail($player_email);
        $invitation->setStatus($status);
        $invitation->setToken($token);

        $em = $this->entityManager;
        $em->persist($invitation);
        $em->flush();

        return $invitation;
    }
}
