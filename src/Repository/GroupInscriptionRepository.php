<?php

namespace App\Repository;

use App\Entity\GroupInscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method GroupInscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroupInscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroupInscription[]    findAll()
 * @method GroupInscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupInscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, GroupInscription::class);
        $this->entityManager = $entityManager;
    }

    // /**
    //  * @return GroupInscription[] Returns an array of GroupInscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GroupInscription
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findOneByUser($user): ?array
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.User = :user')
            ->setParameter('user', $user)
            ->orderBy('g.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }



    public function findOneByUserCampaign($user, $campaign): ?array
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.User = :user')
            ->setParameter('user', $user)
            ->andWhere('g.Campaign = :campaign')
            ->setParameter('campaign', $campaign)
            ->getQuery()
            ->getScalarResult()
        ;
    }


    public function save($group, $user, $isverified, $joined): ?GroupInscription
    {
 
        $newGroupInscription = new GroupInscription();              
        $newGroupInscription->setCampaign($group);
        $newGroupInscription->setUser($user);
        $newGroupInscription->setIsVerified($isverified);
        $newGroupInscription->setJoined($joined);
        
        $em = $this->entityManager;
        $em->persist($newGroupInscription);
        $em->flush();    
        
        return $newGroupInscription;
    }
}
